# An Examination of Mobile Phone Pointing in Surface MappedSpatial Augmented Reality Analysis Scripts

This repository contains the data and scripts used in the following publication

```
Hartmann, J., Vogel, D. 2021. An Examination of Mobile Phone Pointing in Surface MappedSpatial Augmented Reality. International journal of human-computer studies.
```


NOTE: Updates to repository soon.
